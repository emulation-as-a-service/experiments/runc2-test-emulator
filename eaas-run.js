import {exec} from "os";

const args = globalThis.scriptArgs;
const config = JSON.parse(args[1]);

console.log(config);

const drivePath = config.drives[0].path;

exec(["xterm", "-e", "sh", "-c", "hexdump -C \"$1\" | less", "sh", drivePath]);
