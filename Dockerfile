from registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="runc2"
LABEL "EAAS_EMULATOR_VERSION"="${VERSION}"

run sed -i.bak "/^# deb .*partner/ s/^# //" /etc/apt/sources.list 

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes \
unzip \
vde2 \
libvde-dev libvdeplug-dev libvdeplug2 libvde0 \
inotify-tools \
jq \
xterm

run mkdir /libexec \
  && curl -o /libexec/eaas-proxy-download.zip -L "https://gitlab.com/emulation-as-a-service/eaas-proxy/-/jobs/artifacts/master/download?job=build" \
  && cd /libexec \
  && unzip eaas-proxy-download.zip \
  && mv eaas-proxy/eaas-proxy vdenode \
  && rm -r eaas-proxy \
  && mv vdenode eaas-proxy \
  && chmod +x eaas-proxy \
  && rm eaas-proxy-download.zip

workdir /
copy eaas-run .
copy eaas-run.js .
copy setup .
run /setup

COPY runc2.xml /metadata/templates/

COPY metadata /metadata/

ARG OCI_URL
ARG VERSION
RUN jq '.ociSourceUrl = env.OCI_URL' /metadata/metadata.json > /metadata/metadata.json.new && \
  mv /metadata/metadata.json.new /metadata/metadata.json
